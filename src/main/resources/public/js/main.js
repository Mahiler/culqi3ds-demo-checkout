import config, { customerInfo } from "./config/index.js";
import "./config/checkout.js";
import "./config/culqi3ds.js";
import { generateChargeImpl, createCustomerImpl, createCardImpl } from "./services/impl/index.js";
import { formatAmount } from "./helpers/index.js";
import * as selectors from "./helpers/selectors.js";

const deviceId = await Culqi3DS.generateDevice();
if(!deviceId) {
    console.log("Ocurrio un error al generar el deviceID");
}

let tokenId, email, paymenType, customerId = null;
window.addEventListener("message", async function (event) {
    if (event.origin === window.location.origin) {
        const { parameters3DS, error } = event.data;

        if (parameters3DS) {
            let statusCode = null;
            if (paymenType === "unique") {
                const responseCharge = await generateChargeImpl({ deviceId, email, tokenId, parameters3DS });
                statusCode = responseCharge.statusCode;

            } else {
                const responseCard = await createCardImpl({ customerId, tokenId, parameters3DS });
                statusCode = responseCard.statusCode;

            }

            selectors.loadingElement.style.display = "none"

            if (statusCode === 201) {
                selectors.paymentSuccessElement.style.display = "block"
                Culqi3DS.reset();

            } else {
                selectors.paymentFailElement.style.display = "block"
                Culqi3DS.reset();
            }
        }

        if (error) {
            console.log("Ocurrio un error", error);
            selectors.loadingElement.style.display = "none"
        }
    }
},
    false
);



window.culqi = async () => {
    if (Culqi.token) {


        Culqi.close();
        tokenId = Culqi.token.id;
        email = Culqi.token.email;

        selectors.paymentButtonElement.disabled = true;
        selectors.loadingElement.style.display = "block"

        if (paymenType === "unique") {
            const { statusCode } = await generateChargeImpl({ deviceId, email, tokenId });

            validationInit3DS({email, statusCode, tokenId});

        } else {

            selectors.customersNameElement.value = customerInfo.firstName;
            selectors.customersLastNameElement.value = customerInfo.lastName;
            selectors.customersEmailElement.value = email;
            selectors.customersAddressElement.value = customerInfo.address;
            selectors.customersPhoneElement.value = customerInfo.phone;

            selectors.customerFormElement.style.display = "block";

            const { data: responseCustomer, statusCode } = await createCustomerImpl({ ...customerInfo, email });

            if (statusCode === 201) {
                customerId = responseCustomer.id;
                const { statusCode } = await createCardImpl({ customerId, tokenId });

                validationInit3DS({email, statusCode, tokenId});

            } else {
                selectors.loadingElement.style.display = "none"
                selectors.customerCreationFailElement.style.display = "block"
            }
        }

    } else {
        console.log(Culqi.error);
        alert(Culqi.error.user_message);
    }
};


const validationInit3DS = ({ statusCode, email, tokenId }) => {
    if (statusCode === 200) {

        Culqi3DS.settings = {
            charge: {
                totalAmount: config.TOTAL_AMOUNT,
                returnUrl: "http://localhost:8080/"
            },
            card: {
                email: email,
            }
        };
        Culqi3DS.initAuthentication(tokenId);

    } else if (statusCode === 201) {

        selectors.loadingElement.style.display = "none"
        selectors.paymentSuccessElement.style.display = "block"
        Culqi3DS.reset();

    } else {

        selectors.loadingElement.style.display = "none"
        selectors.paymentFailElement.style.display = "block"
        Culqi3DS.reset();

    }
}

selectors.paymentButtonElement.addEventListener("click", (e) => {
    Culqi.open();
    e.preventDefault();
});

selectors.radioButtonElements.forEach((elem) => {
    elem.addEventListener("change", function (event) {
        paymenType = event.target.value;
        selectors.paymentButtonElement.disabled = false;
    });
});

const prefixLabelButton = selectors.paymentButtonElement.textContent.split("  ")[0];
selectors.paymentButtonElement.innerHTML = `${prefixLabelButton} ${formatAmount(config.TOTAL_AMOUNT)} `

