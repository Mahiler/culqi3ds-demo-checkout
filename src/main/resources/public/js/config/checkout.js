import config from "./index.js";

Culqi.publicKey = config.PUBLIC_KEY;
Culqi.settings({
    title: 'Culqi 3DS TEST',
    currency: config.CURRENCY,
    description: 'Polo/remera Culqi lover',
    amount: config.TOTAL_AMOUNT
});