export default Object.freeze({
    TOTAL_AMOUNT: 300,
    CURRENCY: "PEN",
    PUBLIC_KEY: "pk_test_4596bc596a11adbd",
    COUNTRY_CODE: "PE"
});

export const customerInfo = {
    firstName: "Fernando",
    lastName: "Chullo",
    address: "Coop. Villa el Sol",
    phone: "945737476",
}